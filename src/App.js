import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Leaderboard from './Leaderboard';
import Euro2024Teams from './Euro2024Teams.js'; // Assuming you have this component
import ConnectPng from './AppliedLogo.svg';
import './App.css';

const styles = {
  header: {
    height: '72px',
    boxShadow: '0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px rgba(0, 0, 0, 0.14), 0px 1px 3px rgba(0, 0, 0, 0.12)',
    backgroundColor: 'white',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: '10px',
    marginBottom: '20px', // added this line to create space below the header
  },
  icon: {

  },
  title: {
    margin: '0 auto',
  },
};

function App() {
  return (
    <div className="App">
      <header style={styles.header}>
        <img className='connectPng' src={ConnectPng} alt="Logo" style={styles.icon} />
        <h1 style={styles.title}>Applied Sweepstake Euro 2024</h1>
      </header>
      <BrowserRouter basename='/eqapidev/sweepstake/'>
        <div className="container">
          <div className="row">
            <div className="col-lg-8 offset-lg-2">
              <Routes>
                <Route path="/selection" element={<Euro2024Teams iterations={10} />} />
                <Route path="/" element={<Leaderboard />} />
              </Routes>
            </div>
          </div>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;