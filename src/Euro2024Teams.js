
import React, { useState, useEffect } from 'react';

const Euro2024Teams = () => {
    const teams = ["Austria", "Belgium", "Croatia", "Czech Republic", "Denmark", "England", "Finland", "France", "Germany", "Hungary", "Italy", "Netherlands", "North Macedonia", "Poland", "Portugal", "Russia", "Scotland", "Slovakia", "Spain", "Sweden", "Switzerland", "Turkey", "Ukraine", "Wales"];
    const [groups, setGroups] = useState([]);
    const [names, setNames] = useState(''); // Default value

    useEffect(() => {
        let newGroups = [];
        let nameList = names.split(/[\n,]+/);
        for (let i = 0; i < nameList.length; i++) {
            let team1, team2;

            do {
                team1 = teams[Math.floor(Math.random() * teams.length)];
                team2 = teams[Math.floor(Math.random() * teams.length)];
            } while (team1 === team2 || newGroups.some(group => (group.team1 === team1 && group.team2 === team2) || (group.team1 === team2 && group.team2 === team1)));

            newGroups.push({name: nameList[i].trim(), team1, team2});
        }
        setGroups(newGroups);
    }, [names]);

    const handleInputChange = (event) => {
        setNames(event.target.value);
    };

    return (
        <div>
            <label>
                Enter names (separated by commas or new lines):
                <br/>
                <textarea value={names} onChange={handleInputChange} style={{ width: '100%', height: '200px', resize: 'none' }} />
            </label>
            <table className="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Team 1</th>
                        <th>Team 2</th>
                    </tr>
                </thead>
                <tbody>
                    {groups.map((group, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{group.name}</td>
                            <td>{group.team1}</td>
                            <td>{group.team2}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default Euro2024Teams;