import React, { Component } from 'react';
import Papa from 'papaparse';
import csvFile from './Euros2024Sweepstake.csv';

class Leaderboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      leaderboardData: [],
      search: '',
    };
  }

  componentDidMount() {
    Papa.parse(csvFile, {
      download: true,
      header: true,
      complete: (results) => {
        const sortedData = results.data.sort((a, b) => b.TotalPoints - a.Score);
        this.setState({ leaderboardData: sortedData });
      }
    });
  }

  handleSearch = (event) => {
    this.setState({ search: event.target.value });
  }

  render() {
    const { leaderboardData, search } = this.state;
    const filteredData = leaderboardData.filter(entry => 
      entry.Participants.toLowerCase().includes(search.toLowerCase())
    );

    return (
      <div className="container">
        <input 
          type="text" 
          placeholder="Search by name" 
          value={search} 
          onChange={this.handleSearch} 
          className="form-control mb-3"
        />
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Position</th>
              <th>Participants</th>
              <th>Team 1</th>
              <th>Team 2</th>
              <th>Total Positive Points</th>
              <th>Total Negative Points</th>
              <th>Total Points</th>
            </tr>
          </thead>
          <tbody>
            {filteredData.map((entry) => {
              const position = leaderboardData.findIndex(e => e.Participants === entry.Participants) + 1;
              return (
                <tr key={position}>
                  <td>{position}</td>
                  <td>{entry.Participants}</td>
                  <td>{entry.Team1}</td>
                  <td>{entry.Team2}</td>
                  <td>{entry.TotalPositivePoints}</td>
                  <td>{entry.TotalNegativePoints}</td>
                  <td>{entry.TotalPoints}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Leaderboard;